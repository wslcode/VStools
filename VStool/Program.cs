﻿using System;
using System.Diagnostics;
using System.IO;
using System.Reflection;

namespace VStool
{
    class Program
    {
        static String arm_path = @"C:\Keil_v5\ARM\ARMCC\include";
        static String keil_file = null;
        static String base_path = System.Environment.CurrentDirectory;
        static String cmd = base_path+"\\keil2vs.exe ";
        static String argss = "";
        static void Main(string[] args)
        {
            if(args.Length >=1)
            {
                Console.WriteLine("VSTool捕捉到一个外部参数载入,将用于keil系统库头文件路径!\n默认路径：" + arm_path+"\n更正路径: "+args[0]+"\n");
                arm_path = args[0];
            }
            DirectoryInfo root = new DirectoryInfo(base_path);
            if (File.Exists(base_path + "\\keil2vs.exe") == false)
            {
                Console.WriteLine(base_path + "\\keil2vs.exe"+"\n转换组件keil2vs.exe不存在！请检查当前目录是否存在此文件!");
                Console.Read();
                return;
            }
            foreach( FileInfo info in root.GetFiles())
            {
                if (info.Extension == ".sln" || info.Extension == ".uvproj" || info.Extension == ".dsw" || info.Extension == ".dsp" || info.Extension == ".vcxproj" || info.Extension == ".filters" || info.Extension == ".htm" || info.Extension == ".sdf")
                {
                    Console.WriteLine("delete file :"+info.Name);
                    info.Delete();
                        
                }
                if(info.Extension == ".uvprojx")
                {
                    Console.WriteLine("发现项目高版本项目文件 : "+info.FullName);
                    keil_file = info.FullName.ToString();
                }
            }
            //删除完成相关的旧文件
            File.Copy(keil_file, keil_file.Substring(0, keil_file.Length - 1));
            Console.WriteLine("重建项目文件完成!");

            String []p_name = keil_file.Split('\\');
            //p_name = p_name[p_name.Length - 1].Split('.');

            argss += p_name[p_name.Length - 1].Substring(0x00, p_name[p_name.Length - 1].Length - 1) + " ";
            argss += keil_file.Substring(0, keil_file.Length - 1) + " ";
            argss += arm_path + " ";

            Process process = new Process();//创建进程对象  
            ProcessStartInfo startInfo = new ProcessStartInfo();
            startInfo.FileName = cmd;//设定需要执行的命令  
            startInfo.Arguments = argss;//“/C”表示执行完命令后马上退出  
            startInfo.UseShellExecute = false;//不使用系统外壳程序启动  
            startInfo.RedirectStandardInput = false;//不重定向输入  
            startInfo.RedirectStandardOutput = true; //重定向输出  
            startInfo.CreateNoWindow = false;//不创建窗口  
            process.StartInfo = startInfo;
            try
            {
                if (process.Start())//开始进程  
                {
                    process.WaitForExit();//这里无限等待进程结束   
                }
            }
            catch(Exception ess)
            {
                Console.WriteLine(ess.Message);
            }
            File.Delete(keil_file.Substring(0, keil_file.Length - 1));

            Console.WriteLine("转换成功");  
            Console.Read();
        }
    }
}
